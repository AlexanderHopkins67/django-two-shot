from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from receipts.forms import (
    CreateReceiptForm,
    CreateAccountForm,
    CreateExpenseForm,
)


@login_required(login_url="/accounts/login/")
def all_receipts_view(request):
    all_receipts = Receipt.objects.filter(purchaser=request.user)

    context = {"receipts": all_receipts}

    return render(request, "renders/all_receipts.html", context)


@login_required(login_url="/accounts/login/")
def create_receipt_view(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)

        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = CreateReceiptForm()

        context = {
            "form": form,
        }

        return render(request, "renders/create_receipt.html", context)


@login_required(login_url="/accounts/login/")
def all_expenses_view(request):
    all_expenses = ExpenseCategory.objects.filter(owner=request.user)

    context = {"expenses": all_expenses}

    return render(request, "renders/all_expenses.html", context)


@login_required(login_url="/accounts/login/")
def all_accounts_view(request):
    all_accounts = Account.objects.filter(owner=request.user)

    context = {"accounts": all_accounts}

    return render(request, "renders/all_accounts.html", context)


@login_required(login_url="/accounts/login/")
def create_expense_view(request):
    if request.method == "POST":
        form = CreateExpenseForm(request.POST)

        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")

    else:
        form = CreateExpenseForm()

        context = {
            "form": form,
        }

        return render(request, "renders/create_expense.html", context)


@login_required(login_url="/accounts/login/")
def create_account_view(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)

        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("account_list")

    else:
        form = CreateAccountForm()

        context = {
            "form": form,
        }

        return render(request, "renders/create_account.html", context)
