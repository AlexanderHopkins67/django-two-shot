from django.urls import path
from receipts.views import (
    all_receipts_view,
    create_receipt_view,
    all_accounts_view,
    all_expenses_view,
    create_expense_view,
    create_account_view,
)

urlpatterns = [
    path("", all_receipts_view, name="home"),
    path("create/", create_receipt_view, name="create_receipt"),
    path("categories/", all_expenses_view, name="category_list"),
    path("accounts/", all_accounts_view, name="account_list"),
    path("categories/create/", create_expense_view, name="create_category"),
    path("accounts/create/", create_account_view, name="create_account"),
]
